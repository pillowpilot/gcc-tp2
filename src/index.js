const express = require('express');
var bodyParser = require('body-parser');
const app = express();

require('dotenv').config();

const auth_method = require('./auth');

// Middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// View Engine Config
app.set('views', './src/views');
app.set('view engine', 'pug');
var pug = require('pug');

// Static files
app.use(express.static('public'));

// Routes
app.post('/', (req, res) => {
    const email = req.body.email;
    const password = req.body.password;

    const loginStatus = auth_method(email, password);
    
    res.render('login', {loginStatus: loginStatus});
});

app.get('/', (req, res) => {
    res.render('login');
});

app.get('*', (req, res) => {
    res.render('not_found');
});

// Listener
const port = process.env.PORT | 3000;
app.listen(port, () => { console.log('Listening at ' + port); });
