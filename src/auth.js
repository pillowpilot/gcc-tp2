'use strict';
module.exports = (email, password) => {
    const valid_email = process.env.AUTH_EMAIL;
    const valid_password = process.env.AUTH_PASS;
    if( email.localeCompare(valid_email) == 0 && password.localeCompare(valid_password) == 0 ) return true;
    else return false;
};
