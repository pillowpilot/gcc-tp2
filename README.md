Segundo Trabajo Practico de GCC 2019
Prueba de concepto de un formulario de autenticacion en NodeJS.
Carlos Federico Gaona

La Integración Continua se realiza con Gitlab CI mediante el clásico archivo .gitlab-ci.yml.

Se divide en 2 pasos: build y test.

Durante build se instala las dependencias mediante npm install.

Se mantiene el directorio node_modules en la cache para poder realizar el siguiente paso.

Durante test se ejecutan los test unitarios.

App en Heroku: https://gcc-tp2.herokuapp.com/
Las credenciales son carlos.federico@gmail.com:password.