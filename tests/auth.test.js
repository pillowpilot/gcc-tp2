const auth_method = require('../src/auth');

require('dotenv').config();

test('positive test for auth method', () => {
    expect(auth_method(process.env.AUTH_EMAIL, process.env.AUTH_PASS)).toBe(true);
});

test('email incorrect test for auth method', () => {
    expect(auth_method('someIncorrectEmail@hotmail.com', process.env.AUTH_PASS)).toBe(false);
});

test('password incorrect test for auth method', () => {
    expect(auth_method(process.env.AUTH_EMAIL, 'incorrectPassword')).toBe(false);
});

test('email and password incorrect test for auth method', () => {
    expect(auth_method('someIncorrectEmail@hotmail.com', 'incorrectPassword')).toBe(false);
});

test('malformed email for auth method', () => {
    expect(auth_method('someRandomString', 'aPassword')).toBe(false);
});


     
